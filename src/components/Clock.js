import React, { Component } from 'react';
import './Clock.css';
import moment from 'moment'

class Watch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      interval: null
    }
  }

  clock() {
    return () => this.setState({ time: moment().format('LTS') });
  }

  componentDidMount() {
    this.setState({
      interval: setInterval(this.clock(), 1000)
    });
  }

  render() {
    return (
      <div className="watch">
        {this.state.time}
      </div>
    )
  }

  compoenetWillUnmount() {
    clearInterval(this.state.interval);
    this.setState({
      interval: null
    });
  }

}

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true
    }
  }

  changeShow() {
    this.setState({
      show: !this.state.show
    });
  }

  render() {
    let watch;
    this.state.show ? watch = <Watch /> : watch = null;
    return (
      <div className="clock" >
        {watch}
        < button onClick={() => this.changeShow()}> start</button >
        <button onClick={() => this.changeShow()}>stop</button>
      </div >
    );
  }
}

export default Clock;
